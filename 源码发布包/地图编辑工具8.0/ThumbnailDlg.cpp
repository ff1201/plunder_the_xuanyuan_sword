// ThumbnailDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "mapedit.h"
#include "ThumbnailDlg.h"
#include "mapeditView.h"
#include "afxdialogex.h"

#include <atlimage.h>

// CThumbnailDlg 对话框

IMPLEMENT_DYNAMIC(CThumbnailDlg, CDialog)

CThumbnailDlg::CThumbnailDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CThumbnailDlg::IDD, pParent)
{
	m_pView = (CMapeditView*)pParent;
	run_once = TRUE;
}

CThumbnailDlg::~CThumbnailDlg()
{
}

void CThumbnailDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CThumbnailDlg, CDialog)
	ON_BN_CLICKED(IDCANCEL, &CThumbnailDlg::OnBnClickedCancel)
	ON_BN_CLICKED(IDOK, &CThumbnailDlg::OnBnClickedOk)
	ON_WM_PAINT()
	ON_WM_LBUTTONDBLCLK()
	ON_WM_RBUTTONUP()
	ON_COMMAND(ID_JUMP_TO_HERE, &CThumbnailDlg::OnJumpToHere)
	ON_COMMAND(ID_EXPORT_PNG, &CThumbnailDlg::OnExportPng)
	ON_COMMAND(ID_EXPORT_MINI_PNG, &CThumbnailDlg::OnExportMiniPng)
END_MESSAGE_MAP()


// CThumbnailDlg 消息处理程序

void CThumbnailDlg::OnBnClickedCancel()
{
	// TODO: 在此添加控件通知处理程序代码
	CDialog::OnCancel();
}

void CThumbnailDlg::OnBnClickedOk()
{
	// TODO: 在此添加控件通知处理程序代码
	CDialog::OnOK();
}

void CThumbnailDlg::OnPaint()
{
	CPaintDC dc(this); // device context for painting
	int w, h;
	m_pView->GetThumbnailSize(&w, &h);

	if (run_once) {//程序第一次运行所需的操作

		m_MemDC.CreateCompatibleDC(&dc);

		CBitmap memBmp;
		memBmp.CreateCompatibleBitmap(&dc, w, h);
		m_MemDC.SelectObject(&memBmp);

		m_pView->GetThumbnail(&m_MemDC);

		// 根据尺寸改变窗口大小
		CRect rcWindow;
		CRect rcClient;
		GetWindowRect(&rcWindow);
		GetClientRect(&rcClient);
		int cx = w + (rcWindow.Width() - rcClient.Width());
		int cy = h + (rcWindow.Height() - rcClient.Height());

		CPoint center = rcWindow.CenterPoint();
		rcWindow.left = center.x - cx / 2;
		rcWindow.top  = center.y - cy / 2;
		rcWindow.right = rcWindow.left + cx;
		rcWindow.bottom = rcWindow.top + cy;

		MoveWindow (&rcWindow);

		run_once = FALSE;
	}

	dc.BitBlt(0, 0, w, h, &m_MemDC, 0, 0, SRCCOPY);
	SendMessage(WM_NCPAINT, 0, 0);
}

void CThumbnailDlg::OnLButtonDblClk(UINT nFlags, CPoint point)
{
	int w, h;
	m_pView->GetThumbnailSize(&w, &h);

	int x = MAP_GRID_W * point.x / w;
	int y = MAP_GRID_H * point.y / h;
	m_pView->SetCenterPos(x, y);

	OnOK();

	//CDialog::OnLButtonDblClk(nFlags, point);
}

void CThumbnailDlg::OnRButtonUp(UINT nFlags, CPoint point)
{
	m_RButtonUpPoint = point;

	CMenu menu;
	menu.LoadMenu(IDR_POPMENU); //装载右键菜单
	CMenu *popup = menu.GetSubMenu(0);
	CPoint pt;
	GetCursorPos(&pt);
	popup->TrackPopupMenu(TPM_LEFTALIGN|TPM_RIGHTBUTTON,
		pt.x, pt.y, this);//在指定位置显示弹出菜单

	CDialog::OnRButtonUp(nFlags, point);
}

void CThumbnailDlg::OnJumpToHere()
{
	int w, h;
	m_pView->GetThumbnailSize(&w, &h);

	int x = MAP_GRID_W * m_RButtonUpPoint.x / w;
	int y = MAP_GRID_H * m_RButtonUpPoint.y / h;
	m_pView->SetCenterPos(x, y);

	OnOK();
}

void CThumbnailDlg::OnExportPng()
{
	//选择存储文件的位置
	CString filter = "文件 (*.png)|*.png||";	//文件过滤的类型
	CFileDialog fileDlg(false, "png", "*.png", OFN_HIDEREADONLY|OFN_OVERWRITEPROMPT, filter, NULL);
	fileDlg.GetOFN().lpstrInitialDir = m_pView->GetPathName();
	if (IDOK == fileDlg.DoModal()) {
		m_pView->ExportPngFile(fileDlg.GetPathName());
	}
}


void CThumbnailDlg::OnExportMiniPng()
{
	//选择存储文件的位置
	CString filter = "文件 (*.png)|*.png||";	//文件过滤的类型
	CFileDialog fileDlg(false, "png", "*.png", OFN_HIDEREADONLY|OFN_OVERWRITEPROMPT, filter, NULL);
	fileDlg.GetOFN().lpstrInitialDir = m_pView->GetPathName();
	if (IDOK == fileDlg.DoModal()) {

		int w, h;
		m_pView->GetThumbnailSize(&w, &h);

		CImage png;
		png.Create(w, h, 24);
		CDC dc;
		dc.Attach(png.GetDC());
		dc.BitBlt(0, 0, w, h, &m_MemDC, 0, 0, SRCCOPY);

		png.Save(fileDlg.GetPathName());
		dc.Detach();

		png.ReleaseDC();
		png.Destroy();
	}
}
